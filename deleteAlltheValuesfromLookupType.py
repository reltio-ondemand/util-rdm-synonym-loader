import base64
import configparser
import csv
import sys

import requests
import json
import datetime as DT
import datetime
import os
import logging

cur_path = os.path.dirname(__file__)

config = configparser.ConfigParser()
config.read('auth.config')

url = "https://auth.reltio.com/oauth/token"
reltioUsername = config.get('reltio_config', 'USERNAME')
reltioPassword = config.get('reltio_config', 'PASSWORD')
oauth_client_id = config.get('reltio_config', 'OAUTH_CLIENT_ID')
oauth_client_secret = config.get('reltio_config', 'OAUTH_CLIENT_SECRET')

logging.basicConfig(filename="RemovedRDMValues.log", format='%(levelname)s - %(asctime)s - %(message)s',
                    datefmt='%d-%b-%y %H:%M:%S', level=logging.INFO, filemode='a')

print('arguments', sys.argv[1:])

rdm_tenant_id = sys.argv[1]
lookup_type = sys.argv[2]

if os.path.exists(sys.argv[2]):
    print("we have lookup")
else:
    print("Starting the process, thank you for providing RDM ID and Lookup type")


# For script testing
#
# rdm_tenant_id = "RDM Tenant ID"
#
# lookup_type = "Lookup name"
#


def reltioAuthenticate():
    url = "https://auth.reltio.com/oauth/token"
    authKey = oauth_client_id + ":" + oauth_client_secret
    querystring = {"username": reltioUsername, "password": reltioPassword, "grant_type": "password"}
    headers = {
        'Authorization': "Basic " + base64.b64encode(authKey.encode()).decode('utf-8')
    }
    response = requests.request("POST", url, headers=headers, params=querystring)
    jResponse = response.json()
    access_token = jResponse['access_token']
    return (response, access_token)


def lookup_typeExistence(reltioToken):
    url = "https://rdm.reltio.com/lookups/" + rdm_tenant_id + "/" + lookup_type

    payload = {}
    headers = {
        'Authorization': "Bearer " + reltioToken,
        'Content-Type': "application/json"
    }

    response = requests.request("GET", url, headers=headers, data=payload)

    # print(response.text.encode('utf8'))
    return response


def dicDelete(reltioToken):
    url = "https://rdm.reltio.com/tasks/" + rdm_tenant_id

    payload = "{\n  \"type\": \"delete\",\n  \"parameters\": {\n    \"type\": \"rdm/lookupTypes/" + lookup_type + "\"\n  }\n}"

    headers = {
        'Authorization': "Bearer " + reltioToken,
        'Content-Type': "application/json"
    }

    response = requests.request("POST", url, headers=headers, data=payload)
    # print(response)
    return response


if __name__ == '__main__':

    response, reltioToken = reltioAuthenticate()
    if response.status_code not in [200, 201, 202]:
        print(str(DT.datetime.now().replace(microsecond=0)) + ": Reltio authentication failed: " + response.text)
        sys.exit()
    else:
        print(str(DT.datetime.now().replace(microsecond=0)) + ': Reltio authentication successful')

    dict_get_response = lookup_typeExistence(reltioToken)
    status_code = json.loads(dict_get_response.text)

    if dict_get_response.status_code not in [200, 201, 202]:
        json_response = dict_get_response.json()
        errorMessage = json_response['errorMessage']
        print(str(DT.datetime.now().replace(microsecond=0)) + " " + errorMessage)
        sys.exit()
    else:
        dict_response = dicDelete(reltioToken)
        j = json.loads(dict_response.text)

        if dict_response.status_code not in [200, 201, 202]:
            json_response = dict_response.json()
            errorMessage = json_response['errorMessage']
            print(str(DT.datetime.now().replace(microsecond=0)) + ": Error occurred: " + errorMessage)
            sys.exit()
        else:
            print(str(DT.datetime.now().replace(
                microsecond=0)) + f": Values from Lookup Type: {lookup_type} were removed successfully")

        print("End of the process")
