# Purpose of the utility
This free download enables you to utilize a set of custom synonym values in conjunction with the tenant's match process for one or more tenants. If you take this approach, the list of synonyms you load and leverage with this utility are intended to replace those used by the in-built Name Dictionary. For more information and background see Reltio Documentation, https://docs.reltio.com/matchmerge/namedictioncleanser.html

# Background on the approach
If you wish to utilize a customized set of synonyms for matching rather than the ones provided in the in-built Name Dictionary, you will modify the cleanse adapter within your tenant L3 configuration, and you will load your list of custom synonyms into a Reltio RDM tenant. As a result, each time the match engine tokenizes a name (typically a first name but you can extend the concept to other attributes as needed), it will leverage the synonyms you have loaded into RDM. You can assemble the list of synonyms within a simple text file. This utility then loads that text file into an RDM Lookup Type within your RDM tenant in the proper manner for use by the match engine. The format of how the data is loaded into RDM for matching purposes is very specific and is a bit different than you might see with other RDM Lookup types and thus this utility simplifies the loading of this data for you.  
  
This download consists of five files:
* synonymLoader.py
* deleteAlltheValuesfromLookupType.py 
* A sample source file, “Synonym_Dictionary_Sample.csv”
* A configuration file, “auth.config” used by the programs

# Instructions for use:

- Place all files listed above into a directory on your computer. Your computer's path declaration should allow you to run python from that directory. (These programs were developed under python 3 but may work with earlier versions as well.) 

- Edit three params accordingly within the file, auth.config. Specifically
```
USERNAME = <your tenant username>
PASSWORD = <your tenant password>
SOURCE_FILE = <path to the source file> (e.g. /Users/johnsmith/downloads/synonym/Synonym_Dictionary_Sample.csv)
```

For example, I created a folder "synonym" under the downloads folder and put all 5 files into it.  


  
- Replace or edit Synonym_Dictionary_Sample.csv with your actual name and/or content. 





# Two functions are provided by this utility:

To load your text file of names into the RDM tenant, execute the following command line 
> python3 synonymLoader.py <RDM Tenant ID> <RDM Lookup Type>

If you wish to delete a previous set so you can load a modified set, execute the following command line
> python3 deleteAlltheValuesfromLookupType.py <RDM Tenant ID> <RDM Lookup Type>

# General Notes and Best Practices
Some users receive an error in regards to the program failing to load the ‘requests’ module. If this occurs, you might try the following to resolve it.
> python3 -m pip install requests

Notice there is no mention of the RDM tenant ID nor the RDM Lookup Type within the auth.config file. You will specify both as command line arguments giving you agility to easily execute these programs against different RDM tenants and Lookup types. (That said, the L3 configuration of your tenant must also specify this information, so the match engine knows where to find it!)

Notes on use of the Source File
Each line of the source file represents a “pool” of names that will all match to each other. For example, line 2 of the sample file declares that dog = cat = cow = chicken = duck. Thus any one of these will show the other four as potential matches (or will auto merge them if your match rule is set to automerge)

The names you place within the Source file MUST be in all lowercase letters, otherwise the match engine will fail to use them properly. 

All names within the source file must be unique, i.e. must never repeat within the file. The utility does not check for this so this is your responsibility.

Any line that contains only a single item will be ignored. 

Once loaded into RDM and your tenant configuration is setup properly to utilize your content, any record within the tenant that should use this content will need to be reindexed and its tokens inserted into the match table. To do this on a single record that already exists, simply edit the record in some small way and save it. For any new record created, this occurs automatically. Or it can be done to the entire tenant using the “Rebuild Match Table” function within the Reltio Console.







End of QuickStart guide

