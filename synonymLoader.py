import base64
import configparser
import datetime as DT
import json
import logging
import os

import requests
import sys

cur_path = os.path.dirname(__file__)

config = configparser.ConfigParser()
config.read('auth.config')

url = "https://auth.reltio.com/oauth/token"
reltioUsername = config.get('reltio_config', 'USERNAME')
reltioPassword = config.get('reltio_config', 'PASSWORD')
oauth_client_id = config.get('reltio_config', 'OAUTH_CLIENT_ID')
oauth_client_secret = config.get('reltio_config', 'OAUTH_CLIENT_SECRET')

logging.basicConfig(filename="PostedRDMValues.log", format='%(levelname)s - %(asctime)s - %(message)s',
                    datefmt='%d-%b-%y %H:%M:%S', level=logging.INFO, filemode='a')

# input from the terminal

print('arguments', sys.argv[1:])

rdm_tenant_id = sys.argv[1]
lookup_type = sys.argv[2]

if os.path.exists(sys.argv[2]):
    print("we have a lookup")
else:
    print("Starting the process, thank you for providing RDM ID and the Lookup type")


# For script testing
#
# rdm_tenant_id = "RDM Tenant ID"
#
# lookup_type = "Lookup name"
#


def reltio_authenticate():
    url = "https://auth.reltio.com/oauth/token"
    auth_key = oauth_client_id + ":" + oauth_client_secret
    querystring = {"username": reltioUsername, "password": reltioPassword, "grant_type": "password"}
    headers = {
        'Authorization': "Basic " + base64.b64encode(auth_key.encode()).decode('utf-8')
    }
    response = requests.request("POST", url, headers=headers, params=querystring)
    access_token = response.json()['access_token']
    return (response, access_token)


def post_values(reltio_token, full_payload_json):
    url = "https://rdm.reltio.com/lookups/" + rdm_tenant_id + "/" + lookup_type

    headers = {
        'Authorization': "Bearer " + reltio_token,
        'Content-Type': "application/json"
    }

    response = requests.request("POST", url, headers=headers, data=full_payload_json)
    # print(response.text.encode('utf8'))
    return response


def no_of_argu(*args):
    # using len() method in args to count
    return (len(args))


def post_payload(payload):
    batch_full_payload = json.dumps(payload)
    response, reltio_token = reltio_authenticate()
    if response.status_code not in [200, 201, 202]:
        print(str(DT.datetime.now().replace(microsecond=0)) + ": Reltio authentication failed: " + response.text)
        sys.exit()
    else:
        print(str(DT.datetime.now().replace(microsecond=0)) + ': Reltio authentication successful')
    post_response = post_values(reltio_token, batch_full_payload)
    if post_response.status_code not in [200, 201, 202]:
        print(str(DT.datetime.now().replace(microsecond=0)) + ": Batch POST failed: " + post_response.text)
        sys.exit()
    else:
        print(str(DT.datetime.now().replace(microsecond=0)) + ': Posted a Batch of 50 records')


if __name__ == '__main__':

    # Verifying if the 'Name_Dictionary_Example_Text_Format' input provided or not

    if config.get('reltio_config', 'SOURCE_FILE', fallback='NoInput') != 'NoInput':
        try:
            source_file = open(config.get('reltio_config', 'SOURCE_FILE'), "r", encoding='utf-8-sig')
        except:
            print("Invalid path/file not found")
            logging.error(config.get('reltio_config', 'SOURCE_FILE') + ' :: ' + f' input file invalid/not found')
            sys.exit()

        dictionary = {}
        for line in source_file:
            line = line.rstrip("\n")
            arr = line.split(",")

            for i in range(len(arr)):
                if i == 0:
                    continue
                if arr[i] == "":
                    continue
                existing_base_names = dictionary.get(arr[i])
                if existing_base_names is None:
                    existing_base_names = []
                    dictionary[arr[i]] = existing_base_names
                existing_base_names.append(arr[0])

        full_payload = []
        canonical_counter = 0
        synonym_counter = 0
        payload = []
        for item in dictionary.items():
            synonym = item[0]
            base_name_values = []
            synonym_values = [{
                'code': synonym,
                'value': synonym,
                'description': 'Predefined Name Dictionary synonym',
                'enabled': True,
                'canonicalValue': False,
                'downStreamDefaultValue': False
            }]
            for base_name in item[1]:
                base_name_values.append({
                    'code': base_name,
                    'value': base_name,
                    'description': 'Predefined Name Dictionary base name',
                    'enabled': True,
                    'canonicalValue': False,
                    'downStreamDefaultValue': False
                })
            base_name_source = {
                'source': 'DICT_BASE_NAME',
                'values': base_name_values
            }
            synonym_source = {
                'source': 'DICT_SYNONYM',
                'values': synonym_values
            }
            source_mappings = [base_name_source, synonym_source]
            record = {
                'tenantId': rdm_tenant_id,
                'type': lookup_type,
                'enabled': True,
                'code': synonym,
                'sourceMappings': source_mappings
            }
            full_payload.append(record)
            synonym_counter = synonym_counter + 1
            canonical_counter = canonical_counter + len(item[1])

            if synonym_counter % 50 == 0:
                post_payload(full_payload)
                full_payload.clear()

        if len(full_payload) != 0:
            post_payload(full_payload)

print("Canonical values loaded:", canonical_counter)
print("Synonym values loaded:", synonym_counter)

print(" *** End of the Script ***")
